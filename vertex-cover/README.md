# Couverture des arêtes par les sommets

Ce répertoire contient des fichiers permettant de résoudre de façon naïve le
problème de la *couverture des arêtes par les sommets* (en anglais, [vertex
cover problem](https://en.wikipedia.org/wiki/Vertex_cover)).

## Fonctionnement

Il y a un `Makefile` inclus dans le répertoire. Les cibles suivantes sont disponibles:

* `make` permet de compiler le code source avec GCC
* `make data` permet de générer des graphes aléatoires pour tester le programme
* `make bench` permet d'évaluer le temps pris par la solution naïve pour
  résoudre le problème sur chacun des graphes générés par la commande
  `make data`
* `make clean` permet de nettoyer les fichiers générés

## Exemple

En entrant successivement les commandes

```sh
make
make data
make bench > examples/times.csv
make plot
```

on devrait obtenir un graphique semblable à celui-ci:

![Temps de calcul selon le nombre de sommets](examples/times.png)

## Dépendances

* [GCC](https://gcc.gnu.org/)
* [Python](https://www.python.org/)
* [Gnuplot](https://gnuplot.org)

## À faire

* Rendre le script Gnuplot plus générique pour qu'il lise n'importe quel
  fichier
