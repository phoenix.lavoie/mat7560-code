# Ordonnancement de tâches

Ce répertoire contient des fichiers permettant de résoudre à l'aide de la
recherche locale un problème d'ordonnancement de tâches.

Plus précisément, étant donnés

* un ensemble de tâches $`J`$ et
* un ensemble de machines $`M`$
* une fonction $`t : J \rightarrow \mathbb{R}_+`$ indiquant, pour chaque tâche,
  le temps pris pour compléter son traitement

on cherche une fonction $`S : J \rightarrow M`$ qui assigne à chaque tâche une
machine de telle sorte que le temps maximum de traitement par machine soit
minimal.

## Fonctionnement

Il y a un `Makefile` inclus dans le répertoire. Les cibles suivantes sont disponibles:

Pour compiler le projet, il suffit d'entrer la commande

```sh
make
```

qui produit un exécutable `bin/jobs-processing`. Ce programme lit un jeu de
données sur l'entrée standard. Par exemple:

```sh
$ bin/jobs-processing < examples/example-1.jobs
5.000000
```

On peut utiliser le mode « verbeux » et utiliser l'algorithme naïf également:

```sh
# vn = verbose naive
$ bin/jobs-processing vn < examples/example-1.jobs
A batch of 6 jobs to be processed by 3 machines whose durations are
  t(j0) = 3.00
  t(j1) = 2.00
  t(j2) = 3.00
  t(j3) = 4.00
  t(j4) = 2.00
  t(j5) = 1.00
Improved optimal duration to 15.000000
Improved optimal duration to 14.000000
Improved optimal duration to 13.000000
Improved optimal duration to 12.000000
Improved optimal duration to 11.000000
Improved optimal duration to 10.000000
Improved optimal duration to 9.000000
Improved optimal duration to 8.000000
Improved optimal duration to 7.000000
Improved optimal duration to 6.000000
Improved optimal duration to 5.000000
5.000000

# va = verbose approximate
$ bin/jobs-processing va < examples/example-1.jobs
A batch of 6 jobs to be processed by 3 machines whose durations are
  t(j0) = 3.00
  t(j1) = 2.00
  t(j2) = 3.00
  t(j3) = 4.00
  t(j4) = 2.00
  t(j5) = 1.00
A schedule of 6 jobs dispatched onto 3 machines as follows:
  Job #0 is assigned to machine #0
  Job #1 is assigned to machine #2
  Job #2 is assigned to machine #2
  Job #3 is assigned to machine #0
  Job #4 is assigned to machine #0
  Job #5 is assigned to machine #0
  Machine #0's duration is 10.000000
  Machine #1's duration is 0.000000
  Machine #2's duration is 5.000000
[...]
A schedule of 6 jobs dispatched onto 3 machines as follows:
  Job #0 is assigned to machine #0
  Job #1 is assigned to machine #2
  Job #2 is assigned to machine #2
  Job #3 is assigned to machine #1
  Job #4 is assigned to machine #1
  Job #5 is assigned to machine #0
  Machine #0's duration is 4.000000
  Machine #1's duration is 6.000000
  Machine #2's duration is 5.000000
6.000000
```

## Évaluation de la performance

* `make data` permet de générer des ensembles de tâches aléatoires pour tester
  le programme
* `make bench` permet de comparer les valeurs obtenues par la solution naïve et
  par la solution approchée lorsqu'on les lance sur le jeu de données généré
  par `make data`
* `make clean` permet de nettoyer les fichiers générés

Ainsi, en entrant successivement les commandes

```sh
make
make data
make bench > examples/times.csv
make plot
```

on devrait obtenir un graphique semblable à celui-ci:

![Temps de calcul selon le nombre de tâches](examples/times.png)

## Dépendances

* [GCC](https://gcc.gnu.org/)
* [Python](https://www.python.org/)
* [Gnuplot](https://gnuplot.org)

## À faire

* Rendre le script Gnuplot plus générique pour qu'il lise n'importe quel
  fichier.
* Modifier le choix du voisin pour qu'il soit le meilleur parmi les voisins et
  non pas juste strictement meilleur que la solution courante.
